﻿using CoolParking.BL.Interfaces;
using System;
using System.IO;
using System.Reflection;
using System.Timers;

namespace CoolParking.Tester
{
    internal class Program
    {
        static void Main(string[] args)
        {
            //using (TimerService aTimer = new TimerService(1000))
            //{
            //    aTimer.Elapsed += OnTimedEvent;
            //    aTimer.Start();
            //    Thread.Sleep(3000);
            //    aTimer.Stop();
            //}

            ITimerService withdrawTimer = new TimerService(Settings.PaymenPeriod);
            ITimerService logTimer = new TimerService(Settings.LoggingPeriod);
            string path = $@"{Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location)}\Transactions.log";
            ILogService logService = new LogService(path);
            ParkingService _parkingService = new ParkingService(withdrawTimer, logTimer, logService);

            _parkingService.AddVehicle(new Vehicle("AA-0001-TT", VehicleType.Truck, 14));
            _parkingService.AddVehicle(new Vehicle("AA-0001-BB", VehicleType.Bus, 15));

            withdrawTimer.Elapsed += (sender, args) =>
            {
                Console.WriteLine($"__________________  Withdraw  __________________");
                Console.WriteLine("Before:");
                foreach (var vehicle in _parkingService.GetVehicles()) { Console.WriteLine("\t" + vehicle); }

                Console.WriteLine("\t\tAfter:");
                foreach (var vehicle in _parkingService.GetVehicles()) { Console.WriteLine("\t\t\t" + vehicle); }

                Console.WriteLine("\t\t\t\tTransactions:");
                foreach (var transactionInfo in _parkingService.GetLastParkingTransactions())
                {
                    Console.WriteLine("\t\t\t\t\t" + transactionInfo);
                }
            };

            withdrawTimer.Start();
            logTimer.Start();
            Console.ReadKey();
        }

        private static void OnTimedEvent(Object source, ElapsedEventArgs e)
        {
            Console.WriteLine("The Elapsed event was raised");
        }
    }
}
