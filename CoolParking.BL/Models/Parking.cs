﻿// TODO: implement class Parking.
//       Implementation details are up to you, they just have to meet the requirements 
//       of the home task and be consistent with other classes and tests.

using System.Collections.Generic;

public class Parking
{
    public int Capacity { get; set; }
    public decimal Balance { get; set; }
    public List<Vehicle> Vehicles { get; set; } = new List<Vehicle>();
    public List<TransactionInfo> Transactions { get; set; } = new List<TransactionInfo>();

    private static Parking instance;

    private Parking()
    {
        Capacity = Settings.ParkingCapacity;
        Balance = Settings.InitialBalanceOfParking;
    }

    public static Parking getInstance()
    {
        if (instance == null)
        {
            instance = new Parking();
        }

        return instance;
    }
}