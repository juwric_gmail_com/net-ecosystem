﻿// TODO: implement class Vehicle.
//       Properties: Id (string), VehicleType (VehicleType), Balance (decimal).
//       The format of the identifier is explained in the description of the home task.
//       Id and VehicleType should not be able for changing.
//       The Balance should be able to change only in the CoolParking.BL project.
//       The type of constructor is shown in the tests and the constructor should have a validation, which also is clear from the tests.
//       Static method GenerateRandomRegistrationPlateNumber should return a randomly generated unique identifier.

using System;
using System.Text.RegularExpressions;

public class Vehicle
{
    public string Id { get; set; }
    public VehicleType VehicleType { get; set; }
    public decimal Balance { get; set; }

    private readonly Regex _regex = new Regex(@"[A-Z]{2}-[\d]{4}-[A-Z]{2}");

    public Vehicle(string Id, VehicleType VehicleType, decimal Balance)
    {
        if (Balance < 0 || !_regex.IsMatch(Id))
        {
            throw new ArgumentException();
        }

        this.Id = Id;
        this.VehicleType = VehicleType;
        this.Balance = Balance;
    }

    public override string ToString()
    {
        return $"Id:{Id} - Balance:${Balance}";
    }

}