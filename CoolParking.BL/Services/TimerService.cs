﻿// TODO: implement class TimerService from the ITimerService interface.
//       Service have to be just wrapper on System Timers.

using CoolParking.BL.Interfaces;
using System;
using System.Timers;

public class TimerService : ITimerService, IDisposable
{
    private Timer myTimer;
    public event ElapsedEventHandler Elapsed;

    public TimerService(double interval)
    {
        myTimer = new Timer(interval * 1000);
        myTimer.Elapsed += OnTimedEvent;
    }

    public double Interval
    {
        get
        {
            return myTimer.Interval;
        }
        set
        {
            myTimer.Interval = value * 1000;
        }
    }

    public void Dispose()
    {
        myTimer.Dispose();
    }

    public void Start()
    {
        myTimer.Start();
    }

    public void Stop()
    {
        myTimer.Stop();
    }

    private void OnTimedEvent(Object source, ElapsedEventArgs e)
    {
        Elapsed?.Invoke(this, null);
    }
}