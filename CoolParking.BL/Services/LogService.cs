﻿// TODO: implement the LogService class from the ILogService interface.
//       One explicit requirement - for the read method, if the file is not found, an InvalidOperationException should be thrown
//       Other implementation details are up to you, they just have to match the interface requirements
//       and tests, for example, in LogServiceTests you can find the necessary constructor format.

using CoolParking.BL.Interfaces;
using System;
using System.IO;

public class LogService : ILogService
{
    private readonly string _path = "";

    public LogService()
    {
        _path = "Transactions.log";
    }

    public LogService(string path)
    {
        _path = path;
    }

    public string LogPath => _path;

    public string Read()
    {
        return File.ReadAllText(_path);
    }

    public void Write(string logInfo)
    {
        File.AppendAllText(_path, logInfo + Environment.NewLine);
    }
}